#!/usr/bin/env python3.8

# Code from: https://github.com/nytr0gen/ecsc-2019-national-writeup/tree/master/super_caesar

s = bytearray(b'bcjac --- YnuNmQPGhQWqCXGUxuXnFVqrUVCUMhQdaHuCIrbDIcUqnKxbPORYTzVCDBlmAqtKnEJcpED --- UVQR')

k1, s, k2 = s.split(b' --- ')

# found shift from https://www.dcode.fr/caesar-cipher
# small letter: 9
# big letter: 2

for i in range(len(s)):
   c = s[i]
   if 65 <= c <= ord('Z'):
      c = 65 + ((c - 65) - 2) % 26
   else:
      c = 97 + ((c - 97) - 9) % 26
   s[i] = c

print(s)
