#!/usr/bin/env python3.7

from pwn import * 
import re

IP = '35.234.92.174' 
PORT = 31030

io = remote(IP, PORT)

txt = io.recvuntil(b'Input:').decode("utf-8") 

print(type(txt))

nr = int(re.findall("\<\<.*\>\>", txt)[0][2:-2], 10)
print(nr, "->", hex(nr))

io.sendline(hex(nr))

txt = io.recvuntil(b'Input:').decode("utf-8") 

hexstr = re.findall("\<\<.*\>\>", txt)[0][2:-2] 
s = "".join([chr(int(x, 16)) for x in re.findall("..", hexstr)]) 
print(hexstr, "->", s) 

io.sendline(s)

txt = io.recvuntil(b'Input:').decode("utf-8") 
bytestream = [int(x, 8) for x in re.findall("\<\<.*\>\>", txt)[0][2:-2].split(" ")] 
s = "".join([chr(x) for x in bytestream]) 
print(bytestream, "->", s) 

io.sendline(s)

flag = io.recvuntil(b'}') 
print(flag)
