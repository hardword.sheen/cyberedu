#!/usr/bin/env python2.7

import socket
import codecs

#define variables for connection
server = '35.234.92.174'
port = 31030

#create a socket and send the first input
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((server, port))

chal1 = s.recv(1024)
ans1 = hex(int(chal1.split("<<")[1].split(">>")[0]))
s.send(ans1 + "\n")

chal2 = s.recv(1024)
ans2 = codecs.decode(chal2.split("<<")[1].split(">>")[0], 'hex')
s.send(ans2 + "\n")

chal3 = s.recv(1024)
ans3 = ''.join(map(lambda x : chr(int(x,8)),chal3.split("<<")[1].split(">>")[0].split()))
s.send(ans3 + "\n")

feed = s.recv(1024)
print feed
s.close()
