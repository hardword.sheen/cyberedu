***[bash]***

*_list_* : `tshark -nn -r ping-pong.pcapng -T fields -e tcp.dstport '(tcp.flags.syn == 1 and tcp.flags.ack == 0)' | tr '\n' ','` 


***[python]***

`print ''.join([chr(i % 10000) for i in` *_list_* `])`
