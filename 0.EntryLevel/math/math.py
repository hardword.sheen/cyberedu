#!/usr/bin/env python2.7

import resource
import sys
import hashlib

# print resource.getrlimit(resource.RLIMIT_STACK)
# print sys.getrecursionlimit()

# will segfault without below lines.
# tested with Ubuntu: pass, OSX: fail
resource.setrlimit(resource.RLIMIT_STACK, [0x10000000, resource.RLIM_INFINITY])
sys.setrecursionlimit(0x100000)

# from decompiler (Ghidra)

def tryharder(param_1):
   if param_1 < 2:
      uVar2 = 1
   else:
      iVar1 = ecscisfun(param_1 + -1)
      uVar2 = iVar1 + (param_1 + 200) * 2
   return uVar2


def ecscisfun(param_1):
   if param_1 < 2:
      uVar2 = 1
   else:
      iVar1 = tryharder(param_1 + -1)
      uVar2 = iVar1 + param_1 * 2 + 0x539
   return uVar2


def eadsvfbdgw(param_1):
   if 1 < param_1:
       iVar1 = eadsvfbdgw(param_1 + -1)
       param_1 = eadsvfbdgw(param_1 + -2);
       param_1 = param_1 + iVar1;
   return param_1


def asberwefreqw(param_1):
   if param_1 < 2:
      uVar2 = 1
   else:
      iVar1 = asberwefreqw(param_1 + -1)
      uVar2 = iVar1 * param_1
   return uVar2


# main

iVar1 = tryharder(8)
iVar2 = eadsvfbdgw(0xf)
iVar3 = eadsvfbdgw(7)
iVar3 = eadsvfbdgw(iVar3)
iVar4 = eadsvfbdgw(8)
iVar4 = eadsvfbdgw(iVar4)
iVar5 = eadsvfbdgw(8)
iVar5 = eadsvfbdgw(iVar5)
iVar6 = asberwefreqw(4)
iVar7 = asberwefreqw(1)
iVar7 = asberwefreqw(iVar7)
iVar7 = eadsvfbdgw(iVar7)
iVar8 = eadsvfbdgw(5)
iVar8 = eadsvfbdgw(iVar8)
local_24 = tryharder(0xb)
local_24 = local_24 + iVar1 + iVar2 + iVar3 + iVar4 + iVar5 + iVar6 + iVar7 + iVar8
local_24 = tryharder(local_24 + -1)

print "ECSC{"+hashlib.sha256(str(local_24)).hexdigest()+"}"

