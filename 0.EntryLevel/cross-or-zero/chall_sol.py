import itertools
import base64

def string_xor(s, key):
    key = key * (len(s) / len(key) + 1)
    return ''.join(chr(ord(x) ^ ord(y)) for (x,y) in itertools.izip(s, key)) 

#flag = ""
#key = ""

#print base64.b64encode(string_xor(flag, key))

enc = "dHNkdktTAVUHAABUA1VWVgIHBAlSBAFTBAMFUwECAgcAAAFWAFUFCFMACFFUAwQAVgBSBwQJBVZTAFYGCQYHVQABB1IJTQ=="
enc = base64.b64decode(enc)

key = string_xor(enc[:5],"DCTF{")
print string_xor(enc,key)


